## Puppet Client VagrantFile

### VMs
This Vagrantfile supports 5 VMs (VirtualBox provider)

* db01.gvp.local
* web01.gvp.local
* web02.gvp.local
* lb01.gvp.local
* lb02.gvp.local

### Prereqs

#### Development Machine packages

* Vagrant (tested with Vagrant 2.0.2)

#### Networking

You'll need to configure a virtual network within VirtualBox

Attached to: Internal Network
Name: intnet

#### Puppet Master

A puppet master with fqdn of 'puppet.gvp.local' is expected. This Puppet Master should be it's own VM (not a Vagrant provisioned VM). It's expected that this PM have auto signing enabled. The Virtual Box configuration for the Puppet Master should have 3 network adapters setup.

Adapter 1 - NAT
Adapter 2 - Internal Network (intnet) *This is the network used to communicate with Puppet agents*
Adapter 3 - Host-only Adapter (vboxnet1)

This Puppet Master should be configured and tested ensuring that all classes/modules/roles/profiles are set up. The Puppet infrastrcture is outside the scope of this guide.

#### Provisioning VMs

You can leverage the standard ```$ vagrant up``` to provision all VMs. However this will provision them sequentially (one at a time). See [Vagrant's Official Website](http://www.vagrantup.com) for detailed how tos.

Individual Vms within the Vagrantfile can be provisioned by executing ```$ vagrant up <vmname>```

However if you wish to provision them in parallel the follow command will leverage xargs to spin them up in parallel.

```bash
$ grep config.vm.define Vagrantfile | awk -F'"' '{print $2}' | xargs -P5 -I {} vagrant up {}
```

#### Destroying VMs

```$ vagrant destroy``` will destroy all running VMs

```$ vagrant destroy <vmname>``` will destroy individual VMs

#### Suspending VMs

```$ vagrant suspend <vmname>``` will save the state of indvidual VMs which can be brought back up later

#### Provision bootstrap script

vagrant-data/provision.sh is called by each VM provisioned. It does some basic system tweaking and will curl the puppet agent install script from the Puppet Enterprise Master (assumes PM fqdn is puppet.gvp.local)

#### Important information

* You may want to tweak the VM host names, network IP addresses, etc.
