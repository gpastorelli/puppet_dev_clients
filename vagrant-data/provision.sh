#!/bin/bash

# Disable selinux
echo 0 > /sys/fs/selinux/enforce
sed -i 's/enforcing/disabled/g' /etc/selinux/config
# Stop and disable firewalld
systemctl stop firewalld
systemctl disable firewalld
# Enable password ssh
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
# Append Puppet Master info to hosts file
echo "192.168.33.10    puppet puppetmaster puppet.gvp.local puppetmaster.gvp.local" >> /etc/hosts
# Install the puppet agent
curl -k https://puppet.gvp.local:8140/packages/current/install.bash | sudo bash 
# Stop and Disable puppet
systemctl stop puppet
systemctl disable puppet
# Append to puppet.conf
/opt/puppetlabs/bin/puppet config set use_cached_catalog true --section agent
